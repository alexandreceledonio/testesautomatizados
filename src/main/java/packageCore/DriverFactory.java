package packageCore;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverFactory {

	private static WebDriver driver;
	
	private DriverFactory() {
		
	}
	
	//Cria�ao da instancia do navegador
	public static WebDriver getDriver() {
		//Verifica se existe uma inst�ncia de driver
		if(driver == null) { 
			//Verifica qual tipo de browser est� 'setado' na classe Properties
			switch(Properties.browser) {
			
				case FIREFOX:
					//essa linha de comando é porque não colocamos o driver no Path do S.O
					System.setProperty("webdriver.gecko.driver", "/Users/alexandre.celedonio/Desktop/Driver/geckodriver.exe");
					driver = new FirefoxDriver();
				case CHROME:
					//essa linha de comando é porque não colocamos o driver no Path do S.O
					System.setProperty("webdriver.chrome.driver", "/Users/alexandre.celedonio/Desktop/Driver/chromedriver.exe");
					driver = new ChromeDriver();
			}
			//Maximiza a janela do navegador
			driver.manage().window().maximize();
		}
			
		return driver;
	}
	
	//Fecha a instancia do navegador
	public static void killDriver() {
		//Verifica se n�o existe uma inst�ncia de driver
		if(driver != null) {
			//Fecha o navegador
			driver.quit();
			driver = null;
		}
	}
}
