package packageCore;

import static packageCore.DriverFactory.getDriver;
import static packageCore.DriverFactory.killDriver;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.apache.commons.io.FileUtils;

public class BaseTest {

	//Pega o nome do caso de teste executado
	@Rule
	private TestName testName = new TestName();
	
	//Metodo que sera executado no fim de cada caso de teste e tira um print da tela do navegador
	@After
	public void FinalMethod() throws IOException {
		//Tira o print da tela
		TakesScreenshot screen = (TakesScreenshot) getDriver();
		//Armazena o print da tela em uma variavel
		File arquivo = screen.getScreenshotAs(OutputType.FILE);	
		
		//Cria uma pasta 'screenshots' dentro da pasta 'target' no arquivo do projeto
		//Renomea o 'arquivo' com o nome do caso teste executado
		//Coloca o 'arquivo' dentro da pasta screenshots e salva com a extens�o 'jpg'
		FileUtils.copyFile(arquivo, new File("target" + File.separator + "screenshots" + File.separator + testName.getMethodName()+".jpg"));
		
		//Verifica qual o conte�do da vari�vel 'fechar_browser' e invoca o metodo para fechar o navegador
		if(Properties.FECHAR_BROWSER) {
			killDriver();
		}

	}
}
