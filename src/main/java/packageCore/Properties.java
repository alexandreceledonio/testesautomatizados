package packageCore;

public class Properties {

	public static boolean FECHAR_BROWSER = false;
	
	//Indica qual navegador ser� usado
	public static Browsers browser = Browsers.CHROME;
	
	//Criacao de uma estrutura de dados que pode assumir o tipo 'chrome' ou 'firefox'
	//Ser� usada para 'setar' o navegador que ser� usado
	public enum Browsers{
		CHROME,
		FIREFOX
	}
}
